//
//  TPTableViewCell.h
//  Picker View Test Project
//
//  Created by Lee Pollard on 6/24/15.
//  Copyright (c) 2015 Lee Pollard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *numberLabel;

- (void)setupCellWithNumber:(NSNumber *)number;

@end
