//
//  ViewController.m
//  Picker View Test Project
//
//  Created by Lee Pollard on 6/24/15.
//  Copyright (c) 2015 Lee Pollard. All rights reserved.
//

#import "ViewController.h"
#import "TPPickerView.h"

@interface ViewController ()

@property (nonatomic, strong) TPPickerView *pickerView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor redColor];
    
    self.pickerView = [[TPPickerView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.pickerView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
