//
//  OFPickerViewTestProject.m
//  Picker View Test Project
//
//  Created by Lee Pollard on 6/24/15.
//  Copyright (c) 2015 Lee Pollard. All rights reserved.
//

#import "TPPickerView.h"
#import "TPTableViewCell.h"

static NSString *const kTPTableViewCellIdentifier = @"TPTableViewCell";
const CGFloat kTPTableViewCellHeight = 50.f;
const CGFloat kTPTableViewHeaderHeight = 300.f;

@interface TPPickerView () <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) UITableView *tableView1;
@property (nonatomic, strong) UIScrollView *scrollView1;

@property (nonatomic, strong) NSArray *hourArray;

@property (nonatomic, strong) UIView *bufferView;

@property (nonatomic, assign) CGFloat previousOffset;

@end

@implementation TPPickerView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [self setupBufferView];
    [self setupScrollView];
    [self setupTableView];
    [self setupHourArray];
    
    [self setHourWithIndex:@7];
    [self.tableView1 reloadData];
}

-(void)setupTableView
{
    self.tableView1 = [[UITableView alloc] initWithFrame:self.bounds];
    self.tableView1.dataSource = self;
    self.tableView1.delegate = self;
    self.tableView1.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView1.allowsSelection = NO;
    self.tableView1.backgroundColor = [UIColor blackColor];
    self.tableView1.scrollEnabled = NO;
    [self.tableView1 registerNib:[UINib nibWithNibName:kTPTableViewCellIdentifier bundle:nil]
          forCellReuseIdentifier:kTPTableViewCellIdentifier];
    
    self.tableView1.tableHeaderView = self.bufferView;
    self.tableView1.tableFooterView = self.bufferView;
    
    CGRect frame = self.tableView1.frame;
    frame.size.height += 2 * self.bufferView.frame.size.height;
    self.tableView1.frame = frame;
    
    [self.scrollView1 addSubview:self.tableView1];

}

-(void)setupScrollView
{
    self.scrollView1 = [[UIScrollView alloc] initWithFrame:self.bounds];
    self.scrollView1.backgroundColor = [UIColor blackColor];
    self.scrollView1.delegate = self;
    [self addSubview:self.scrollView1];
}

-(void)setupHourArray
{
    self.hourArray = @[@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12];
    
    self.scrollView1.contentSize = CGSizeMake(self.bounds.size.width, kTPTableViewCellHeight * self.hourArray.count + kTPTableViewHeaderHeight * 2);
}

-(void)setupBufferView
{
    self.bufferView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, (kTPTableViewHeaderHeight))];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.hourArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TPTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTPTableViewCellIdentifier
                                                                 forIndexPath:indexPath];
    [cell setupCellWithNumber:self.hourArray[indexPath.row]];
    cell.backgroundColor = [UIColor clearColor];
    
    NSLog(@"Index #: %ld",(long)indexPath.row);
    
    if ((self.scrollView1.contentOffset.y / kTPTableViewCellHeight) == indexPath.row + 1) {
        cell.numberLabel.alpha = 1;
    }

    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kTPTableViewCellHeight;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // TODO Check if an integer
//    NSLog(@"Content Offset: %f", scrollView.contentOffset.y);
    NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:scrollView.contentOffset.y/ kTPTableViewCellHeight inSection:0];
    
    TPTableViewCell *currentCell = (TPTableViewCell *)[self.tableView1 cellForRowAtIndexPath:currentIndexPath];
    
    currentCell.numberLabel.alpha = 1;
    
    for (NSInteger i = 0; i < self.hourArray.count; i++) {
        if ((scrollView.contentOffset.y / kTPTableViewCellHeight) != currentIndexPath.row) {
            TPTableViewCell *currentCell = (TPTableViewCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            currentCell.numberLabel.alpha = 0.5f;
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
}

#pragma mark - Private Functions

-(void)setHourWithIndex:(NSNumber *)index
{
    // TODO Make sure to check index range
    self.scrollView1.contentOffset = CGPointMake(0, [index floatValue] * kTPTableViewCellHeight);
}


@end
