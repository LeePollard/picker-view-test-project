//
//  TPTableViewCell.m
//  Picker View Test Project
//
//  Created by Lee Pollard on 6/24/15.
//  Copyright (c) 2015 Lee Pollard. All rights reserved.
//

#import "TPTableViewCell.h"

@interface TPTableViewCell ()

@end

@implementation TPTableViewCell

- (void)setupCellWithNumber:(NSNumber *)number
{
    self.numberLabel.textColor = [UIColor whiteColor];
    self.numberLabel.font = [UIFont systemFontOfSize:25.f];
    self.numberLabel.text = [number stringValue];
    self.numberLabel.alpha = 0.5f;
}

@end
